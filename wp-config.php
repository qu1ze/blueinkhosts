<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cl47-a-wordp-7o');

/** MySQL database username */
define('DB_USER', 'cl47-a-wordp-7o');

/** MySQL database password */
define('DB_PASSWORD', 'Y4h^4dj^2');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'wp+#SujryW(3(WeWeu60jo(BFeihxctR0K1=R^yak)d89DdAOGsJPggO(cfyVyzq');
define('SECURE_AUTH_KEY',  '!7iR8+94f=P2Z^2V=Ajhh79KV1tFv#u6BMYQy+)^)bt_RkuFjv5AUOY7188C5WLC');
define('LOGGED_IN_KEY',    '2H/dgnf^RXzCUHB=c(CB2Ur3Z0=uQ=0vUi+NfjUEV=X_6iasrZEg/96rJ1DI5CL(');
define('NONCE_KEY',        'wWHxQ3_2mGKMyC_TD5KIN#2h!HlD/51#1A5F-vQ5Nni5HeDJAfWS6wu+A)0WEVW3');
define('AUTH_SALT',        'u6AivXo#Zt-I3eEkmYVLkhaou_ISp#6u0Sv!l8R)ubHR8KMtWf31QIcyGFcm/GP#');
define('SECURE_AUTH_SALT', 'c/jf#VyM(u3cKfugjg729h05mOuBGEGZFTV=ZCBs5k3bDSWqw^Pnhc_eRgZbIOwM');
define('LOGGED_IN_SALT',   'nKRygnK6DQddnn4DX0uZo2qaQ0u#hM+C2dR8x9YZj2NX(wgZaX5^T(b72xMqbY/D');
define('NONCE_SALT',       'MscGFYsQCpY9rOk2eoRP9yldntzb-CXRjdd)U4)_PVF)ZQ^y7GPHJUJivV+gM(FJ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de.mo to wp-content/languages and set WPLANG to 'de' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/**
 *  Change this to true to run multiple blogs on this installation.
 *  Then login as admin and go to Tools -> Network
 */
define('WP_ALLOW_MULTISITE', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/* Destination directory for file streaming */
define('WP_TEMP_DIR', ABSPATH . 'wp-content/');

