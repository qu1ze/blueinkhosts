<?php if(ci_setting('newsletter_action')!=''): ?>
	<aside class="newsletter hn">
		<h3><span><?php ci_e_setting('newsletter_heading'); ?></span></h3>
		<form method="post" action="<?php ci_e_setting('newsletter_action'); ?>" class="group">
			<p class="newsletter-title"><?php ci_e_setting('newsletter_description'); ?></p>
			<p><input id="<?php ci_e_setting('newsletter_name_id'); ?>" name="<?php ci_e_setting('newsletter_name_name'); ?>" type="text" placeholder="<?php _e('Enter your name', 'ci_theme'); ?>" /></p>
			<p><input id="<?php ci_e_setting('newsletter_email_id'); ?>" name="<?php ci_e_setting('newsletter_email_name'); ?>" type="text" placeholder="<?php _e('Enter your email', 'ci_theme'); ?>" /></p>
			<p class="newsletter-action"><input type="submit" value="<?php _e('Submit', 'ci_theme'); ?>" /></p>
		</form>
	</aside><!-- /newsletter -->
<?php endif; ?>
