jQuery(document).ready(function($) {
	// first run.

	var template_box = $('#page_template');
	var metabox = $('div#ci_page_room_listing_meta');
	metabox.hide();
	if( template_box.val() == 'template-rooms.php' )
		metabox.show();


	// show only the custom fields we need in the post screen
	$('#page_template').change(function(){
	if( template_box.val() == 'template-rooms.php' )
			metabox.show();
		else
			metabox.hide();
	});

}); 
