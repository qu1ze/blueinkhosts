<?php 
define('RATING_MSG',__('Valora este lugar haciendo clic en una estrella:','templatic'));
define('SELECT_ALL',__('Seleccionar todo','templatic'));
define('WORD_VERIFICATION',__('Word verification','templatic'));
/* FOR BREADCRUMB BOF */
define('HOME',__('Inicio','templatic'));
/* FOR BREADCRUMB EOF */
define('LATEST_ENTRIES',__('Ultimas entradas','templatic'));
/*author.php BOF*/
define('BACK_TO_DASHBOARD',__('&laquo; Regresar a mi Panel','templatic'));
define('LISTING_TEXT',__('Listado','templatic'));
define('MY_SUBMISSION',__('Mis envíos','templatic'));
define('DASHBOARD',__('Panel','templatic'));
define('LISTING_NOT_AVAIL_MSG',__('Usted no ha publicado ninguna lista todavía.','templatic'));
define('EDIT_TEXT',__('editar','templatic'));
define('RENEW_TEXT',__('editar','templatic'));
define('DELETE_TEXT',__('borrar','templatic'));
define('ALPHA_ORDER_TEXT','Alfabetico');
define('LATEST_ORDER_TEXT','Ultimos publicados');
define('RANDOM_ORDER_TEXT','Aleatorio');
define('READ_MORE_LABEL',__('Leer más','templatic'));
define('NEXT_TITLE',__('Siguiente','templatic'));
define('PREVIEW_TITLE',__('Anterior','templatic'));
define('SET_ADDRESS_ON_MAP',__('Establezca la dirección en el mapa','templatic'));
define('TAG_SEARCG_TEXT',__('Etiquetas','templatic'));
/*author.php EOF*/

/*header_searchform.php BOF*/
define('NEAR_TEXT',__('Cerca de','templatic'));
define('SEARCH_FOR_TEXT',__('Buscar','templatic'));
define('SEARCH_FOR_MSG',__('pizza&#44; discoteca o municipio','templatic'));
define('SEARCH_NEAR_MSG',__('calle o avenida','templatic'));
define('MSG_SEARCH_ENTER',__('No ha ingresado nada en el cuadro de búsqueda.','templatic'));
/*header_searchform.php EOF*/

/*archive.php BOF */
define('POST_TAG_ARCHIVE_TEXT',__('Posts tagged &quot;%s&quot;','templatic'));
define('DAILY_ARCHIVE_TEXT',__('Daily archive %s','templatic'));
define('MONTHLY_ARCHIVE_TEXT',__('Monthly archive %s','templatic'));
define('YEARLY_ARCHIVE_TEXT',__('Yearly archive %s','templatic'));
define('AUTHOR_ARCHIVE_TEXT',__('Author archive','templatic'));
define('BLOG_ARCHIVE_TEXT',__('Blog archives','templatic'));
/*archive.php EOF */

/* single-place.php BOF */
define('POST_BY',__('Posts por ','templatic'));
define('ON',__('el','templatic'));
define('TAGS_LIST_TEXT',__('Etiquetas','templatic'));
define('VIEW_LIST_TEXT',__('Vistas Totales','templatic'));
define('VIEW_LIST_TEXT_DAILY',__('Vistas Diarias','templatic'));
define('LISITNG_MAP',__('Listing Map','templatic'));
define('RELATED_LISTING',__('Related listing','templatic'));
define('PREVIOUS_POST',__('Post Anterior','templatic'));
define('NEXT_POST',__('Post Siguiente','templatic'));
define('MAIL_TO_FRIEND',__('Enviar a un amigo','templatic'));
define('SEND_INQUIRY',__('Enviar Consulta','templatic'));
define('CLAIM_OWNERSHIP',__('¿Es el dueño de este negocio?','templatic'));
define('TIME',__('Horario','templatic'));
define('PHONE',__('Teléfono','templatic'));
define('EMAIL',__('Email','templatic'));
define('RATING',__('Valoración','templatic'));
define('TWITTER',__('Twitter','templatic'));
define('FACEBOOK',__('Facebook','templatic'));
define('ADDRESS',__('Dirección','templatic'));
define('PRINT1',__('Imprimir','templatic'));
define('SPECIAL_OFFER',__('Oferta Especial','templatic'));
/* single-place.php EOF */

/* single-event.php BOF */
define('HOW_TO_REGISTER',__('Cómo registrar','templatic'));
define('REGISTER_NOW',__('Registrar Ahora','templatic'));
define('FEES_TEXT',__('Honorarios : ','templatic'));
define('PREVIOUS',__('anterior','templatic'));
define('NEXT',__('siguiente','templatic'));
/* single-event.php EOF */

/* taxonomy-placecategory.php BOF */
define('SORT_BY',__('Ordenar por : ','templatic'));
define('ALL',__('Todo','templatic'));
define('REVIEWS',__('Revisiones','templatic'));
define('PINPOINT',__('Pinpoint','templatic'));
define('SHARE_TEXT',__('Compartir','templatic'));
/* taxonomy-placecategory.php BOF */

/*comments.php BOF */
define('PASSWORD_PROTECT',__('Esta entrada está protegida. Introduzca la contraseña para ver los comentarios.','templatic'));
define('WHAT_THINK_TEXT',__('¿Qué te parece?','templatic'));
define('YOU_MUST',__('Usted debe ser','templatic'));
define('LOGGED_IN',__('conectado','templatic'));
define('POST_COMMENT',__('para postear un comentario.','templatic'));
define('LOGGED_AS',__('Conectado como','templatic'));
define('LOGOUT',__('Cerrar sesión &raquo;','templatic'));
define('POST_REVIEW',__('Postear tu Revisión','templatic'));
define('REVIEW',__('Revisión','templatic'));
define('WEBSITE',__('Página Web','templatic'));
define('COMMENT',__('Comentario','templatic'));
define('SUBMIT',__('Enviar','templatic'));
define('REQUIRED',__('Requerido','templatic'));
define('CMT_YOUR_NAME',__('Tu nombre','templatic'));
define('CMT_EMAIL_TEXT',__('E-mail','templatic'));
/*comments.php EOF */

/* loop.php BOF */
define('SEARCH_RESULT_NOT_FOUND',__('Disculpe, pero ningún resultado fue encontrado para esa busqueda. Pruebe con otra palabra.','templatic'));
define('VIEW_TEXT',__('Ver','templatic'));
define('MORE_TEXT',__('Más','templatic'));
//define('By',__('Por','templatic'));
//define('on',__('el','templatic'));
/* loop.php EOF */

/*tpl_advanced_search.php BOF*/
define('SEARCH_WEBSITE',__('Buscar en este Sitio Web','templatic'));
define('SEARCH',__('Buscar','templatic'));
define('CATEGORY',__('Categoria','templatic'));
define('SELECT_CATEGORY',__('Seleccionar categoria','templatic'));
define('DATE_TEXT',__('Fecha','templatic'));
define('TO',__('<span>a</span>','templatic'));
define('AUTHOR_TEXT',__('Autor','templatic'));
define('EXACT_AUTHOR_TEXT',__('Autor exacto','templatic'));
define('SEARCH_ALERT_MSG',__('Por favor, introduzca la palabra que desea buscar','templatic'));
/*tpl_advanced_search.php EOF*/

/* tpl_contact.php BOF */
define('DEAR',__('Estimado ','templatic'));
define('NAME',__('Nombre','templatic'));
define('SUBJECT',__('Asunto','templatic'));
define('MESSAGE',__('Mensaje','templatic'));
define('INQUIRY_TEXT',__('Para consultas adicionales, llene el siguiente formulario','templatic'));
define('CONTACT_SUCCESS_TEXT',__('Su mensaje se ha enviado correctamente.','templatic'));
/* tpl_contact.php EOF */

/*tpl_sitemap.php BOF */
define('PAGE_TEXT',__('Páginas','templatic'));
define('POST_TEXT',__('Posts','templatic'));
define('ARCHIVE_TEXT',__('Archivos','templatic'));
define('CATGORIES_TEXT',__('Categorias','templatic'));
define('META_TEXT',__('Meta','templatic'));
define('RDF_RSS',__('RDF/RSS','templatic'));
define('RSS_TEXT',__('RSS','templatic'));
define('ONE_FEED',__('1.0 feed','templatic'));
define('POINT_FEED',__('0.92 feed','templatic'));
define('TWO_FEED',__('2.0 feed','templatic'));
define('ATOM_FEED',__('Atom feed','templatic'));
define('TAGS_TEXT',__('Etiquetas','templatic'));
define('TAGS_TEXT_HEAD',__('Etiquetas','templatic'));
/*tpl_sitemap.php EOF */

/*search.php BOF */
define('CATEGORY_TEXT',__('Categorias','templatic'));
define('SEARCH_CATEGORY_TITLE',__('Resultados de búsqueda por categoria','templatic'));
define('SEARCH_AUTHOR_TITLE',__('Resultados de búsqueda por autor','templatic'));
define('SEARCH_DATE_TITLE',__('Resultados de búsqueda por fecha','templatic'));
define('SEARCH_TITLE',__('Resultados de búsqueda por ','templatic'));
/*search.php EOF */

/*Send inquiry module BOF */
/*popup_owner_frm.php BOF */
define('INQ_YOUR_NAME',__('Nombre completo','templatic'));
define('INQ_YOUR_EMAIL',__('Su email','templatic'));
define('INQ_CONTACT',__('Número de contacto','templatic'));
define('INQ_SUBJECT',__('Asunto','templatic'));
define('INQ_MSG',__('Mensaje','templatic'));
define('INQ_SUB_BTN',__('Enviar','templatic'));
define('INQ_FRM_TITLE',__('Consulta','templatic'));
define('INQ_DEFAULT_MSG',__('Hola,

Me gustaría investigar más sobre este anuncio. Por favor déjeme saber como puedo establecer contacto con usted. Espero su pronta respuesta','templatic'));
/*popup_owner_frm.php EOF */
/*End of Send inquiry module BOF */

/*Footer BOF */
define('YEAR_TEXT',__('2013','templatic'));
define('RIGHTS_TEXT',__('Todos los derechos reservados.','templatic'));
define('DESIGNED_TEXT',__('Diseñado por:','templatic'));
define('TEMPLATIC_TEXT',__('RyG Soluciones','templatic'));
/*Footer EOF */
//header.php
define('HEADER_ADD_PLACE_SEO',__('Add place listing'));
define('HEADER_ADD_EVENT_SEO',__('Add event listing'));
define('HEADER_ADD_PREVIEW_SEO',__('Add listing preview'));
define('HEADER_LOGIN_REGISTRATION_SEO',__('Site Registration and Login Form'));
define('HEADER_SUCCESS_PAGE_SEO',__('Exito!'));
//submit_event.php
define('EVENT_REGISTRATION_DESC',__('How to register'));
define('EVENT_REGISTRATION_FEES',__('Registration fees'));
define('EVENT_REGISTRATION_DESC_MSG',__('Enter how to register details '));
$currency = get_option('currency');
$currencysym = get_option('currencysym');
define('EVENT_REGISTRATION_FEES_MSG',__('Enter registration fees, in '.$currency.' eg. : <b>'.$currencysym.'50</b>','templatic'));
define('FEATURED_TEXT',__('Would you like to make this place featured?','templatic'));
define('FEATURED_MSG',__('An additional amount will be charged to make this place featured.You have the option to feature your place on home page or category page or both.','templatic'));
define('TOTAL_TEXT',__('Total price as per your selection.','templatic'));


define('INDICATES_MANDATORY_FIELDS_TEXT',__('Indicates mandatory fields'));
define('POST_EVENT_TITLE',__('Agregar evento','templatic'));
define('RENEW_EVENT_TEXT',__('Renovar evento','templatic'));
define('EDIT_EVENT_TEXT',__('Actualizar evento','templatic'));
define('POST_PLACE_TITLE',__('Agregar lugar','templatic'));
define('RENEW_LISING_TEXT',__('Renovar place','templatic'));
define('EDIT_LISING_TEXT',__('Actualizar place','templatic'));
define('EVENT_DATE',__('Fecha','templatic'));
define('EVENT_TIME',__('Hora','templatic'));

define('IAM_TEXT',"Soy");
define('LOGINORREGISTER',"Iniciar o registrar");
define('EXISTING_USER_TEXT',"Usuario existente");
define('NEW_USER_TEXT',"¿Usuario nuevo? Registrese!");
define('LOGIN_TEXT',__('Login'));
define('PASSWORD_TEXT',__('Contraseña','templatic'));
define('SUBMIT_BUTTON',__('Enviar','templatic'));
define('PRO_PHOTO_TEXT',__('Añadir imágen: <small>(Puede subir varias para crear una galería en la página de detalle)</small>','templatic'));
define('PHOTOES_BUTTON',__('Seleccionar Imágenes','templatic'));
define('PRO_DESCRIPTION_TEXT',__('Descripción de Listado'));
define('EVENT_DESCRIPTION_TEXT',__('Descripción del Evento'));
define('PRO_FEATURE_TEXT',__('Ofertas Especiales'));
define('CONTACT_TEXT',__('Contact'));
define('PRO_ADD_COUPON_TEXT',__('Enter Coupon Code'));
define('COUPON_NOTE_TEXT',__('Enter coupon code here (optional)'));
define('CAPTCHA_TITLE_TEXT',__('Captcha Verification'));
define('SELECT_TYPE_TEXT',__('Select Package Type'));
define('COUPON_CODE_TITLE_TEXT',__('Coupon Code'));

define('PUBLISH_DAYS_TEXT',__('%s : number of publish days are %s (<span id="%s">%s %s</span>)'));
define('SELECT_PAY_MEHTOD_TEXT',__('Select payment method'));


define('GOING_TO_PAY_MSG',__('Esta es una vista previa de su anuncio que todavía no está publicado. <br />Puede volver atrás y editar los detalles o hacer clic en &lsquo;Pagar &amp; Publicar&rsquo;.<br/>Monto a pagar: <b>%s</b> con validez <b>%s</b> dias | Paquete Seleccionado: <b>%s</b>'));
define('GOING_TO_FREE_MSG',__('Esta es una vista previa de su anuncio que todavía no está publicado. <br />Puede volver atrás y editar los detalles o hacer clic en &lsquo;Pagar &amp; Publicar&rsquo;.<br/>Paquete Seleccionado: <b>%s</b> con validez <b>%s</b> dias| Monto a pagar: <b>%s</b>'));

define('GOING_TO_UPDATE_MSG',__('Esta es una vista previa de su anuncio que todavía no se ha actualizado. <br />Puede volver atrás y editar los detalles o hacer clic en &lsquo;Actualizar&rsquo;.'));


define('WRONG_COUPON_MSG',__('Código de Cupón Invalido'));

define('EMAIL_USERNAME_EXIST_MSG',__('Email ya registrado. Por favor elija un Email diferente.'));
define('REGISTRATION_DESABLED_MSG',__('Registro de nuevos usuarios deshabilitado.'));

define('SELECT_PACKAGE_TEXT',__('Seleccionar Paquete','templatic'));
define('SET_GOOGLE_MAP',__('Set Google Map','templatic'));
define('CAPTCHA',__('Word Verification','templatic'));
define('PRO_PREVIEW_BUTTON',__('Review your listing','templatic'));
define('EVENT_INFO_TEXT',__('Listing information','templatic'));
define('SELECT_LISTING_TYPE_TEXT',__('Select place type','templatic'));
define('SELECT_EVENT_TYPE_TEXT',__('Select Type','templatic'));
define('EVENT_TITLE_TEXT',__('Place Title','templatic'));
define('IMAGE_ORDERING_MSG',__('Note : You can sort images from Dashboard and then clicking on "Edit" in the listing','templatic'));
define('CONTACT_NAME_TEXT',__('Usuario','templatic'));
define('CITY_TEXT',__('Ciudad','templatic'));
define('STATE_TEXT',__('Provincia','templatic'));
define('MOBILE_TEXT',__('Número Celular','templatic'));
define('EMAIL_TEXT',__('Email','templatic'));
define('EMAIL_TEXT_MSG',__('Enter valid Email else you will face an error on the next page.','templatic'));
define('WEBSITE_TEXT',__('Website','templatic'));
define('TWITTER_TEXT',__('Twitter','templatic'));
define('FACEBOOK_TEXT',__('Facebook','templatic'));
define('TAGKW_TEXT',__('Tag Keyword','templatic'));
define('TAGKW_MSG',__('Tags are short keywords, with no space within. Up to 40 characters only.','templatic'));
define('PHOTO_TEXT',__('Subir Imagen','templatic'));
define('BIODATA_TEXT',__('Short Biodata Information','templatic'));
define('IMAGE_TYPE_MSG',__('Note : PNG, GIF of JPEG only, for better image quality upload image size 150x150','templatic'));
define('EVENT_CATETORY_TEXT',__('Category','templatic'));
define('EVENT_ADDRESS',__('Address','templatic'));
define('EVENT_ADDRESS_LAT',__('Address Latitude','templatic'));
define('EVENT_ADDRESS_LNG',__('Address Longitude','templatic'));
define('EVENT_MAP_VIEW_LNG',__('Google Map View','templatic'));
define('EVENT_CITY_TEXT',__('Elegir Ciudad','templatic'));


define('BASIC_INFO_TEXT',__('Home Information','templatic'));
define('PRO_BACK_AND_EDIT_TEXT',__('&laquo; Go Back and Edit','templatic'));
define('PRO_UPDATE_BUTTON',__('Actualizar','templatic'));
define('PRO_SUBMIT_BUTTON',__('Publicar','templatic'));
define('PRO_CANCEL_BUTTON',__('Cancelar','templatic'));
define('PRO_SUBMIT_PAY_BUTTON',__('Pagar & Publicar','templatic'));
define('EVENT_DESCRIPTION',__('Listing Description','templatic'));
define('CONTACT_DETAIL_TITLE',__('Publisher Information','templatic'));
define('EVENT_INFORMATION_TEXT',__('Fill Out Event Information','templatic'));
define('LISTING_DETAILS_TEXT',__('Enter place details','templatic'));

define('EVENT_DETAILS_TEXT',__('Enter Event Details','templatic'));
define('EVENT_TITLE',__('Titulo de Evento','templatic'));
define('EVENT_TITLE_HEAD',__('Evento','templatic'));
define('EVENT_ST_TIME',__('Hora de inicio','templatic'));
define('EVENT_END_TIME',__('Hora de finalización','templatic'));


define('EVENT_CONTACT_INFO',__('Teléfono','templatic'));
define('EVENT_CONTACT_EMAIL',__('Email','templatic'));
define('EVENT_WEBSITE',__('Website','templatic'));
define('PRO_DELETE_PRE_MSG',__('Are you really sure want to DELETE this listing? Deleted listing can not be recovered later','templatic'));
define('PRO_DELETE_BUTTON',__('Yes, Delete Please!','templatic'));
define('IS_A_FEATURE_PRO_TEXT',__('This listing is listed as Featured. Do you want to remove it from feature listing?','templatic'));

define('ADDREDD_MSG',__('Please enter listing address. eg. : <b>230 Vine Street And locations throughout Old City, Philadelphia, PA 19106</b>','templatic'));
define('TIMING_MSG',__('Enter Business or listing Timing Information. <br /> eg. : <b>10.00 am to 6 pm every day</b>','templatic'));
define('GET_LATITUDE_MSG',__('Please enter latitude for google map perfection. eg. : <b>39.955823048131286</b>','templatic'));
define('GET_LOGNGITUDE_MSG',__('Please enter longitude for google map perfection. eg. : <b>-75.14408111572266</b>','templatic'));
define('GET_MAP_MSG',__('Click on "Set Address on Map" and then you can also drag pinpoint to locate the correct address','templatic','templatic'));
define('CONTACT_MSG',__('You can enter phone number,cell phone number etc.','templatic'));
define('WEBSITE_MSG',__('Enter website URL. eg. : <b>http://myplace.com</b>','templatic'));
define('TWITTER_MSG',__('Enter twitter URL. eg. : <b>http://twitter.com/myplace</b>','templatic'));
define('FACEBOOK_MSG',__('Enter facebook URL. eg. : <b>http://facebook.com/myplace</b>','templatic'));
define('CATEGORY_MSG',__('Select listing category from here. Select atleast one category','templatic'));
define('EVENT_MSG',__('Select event category from here. Select atleast one category','templatic'));

/*Dashboard widget text*/
define('RECENT_PLACES_TEXT',__('Lugares Recientes','templatic'));
define('PUBLISHED_TEXT',__('Publicado','templatic'));
define('RECENT_EVENTS_TEXT',__('Eventos Recientes','templatic'));
define('ID_TEXT',__('ID','templatic'));
define('TITLE_TEXT',__('Título','templatic'));
define('AUTHOR_NAME_TEXT',__('Nombre de Autor','templatic'));
define('CLAIMER_TEXT',__('Nombre de Reclamante','templatic'));
define('CONTACT_NUM_TEXT',__('Numero de Contacto','templatic'));
define('ACTION_TEXT',__('Acción','templatic'));
define('SELECT_CITY_TEXT',__('«elegir ciudad»','templatic'));
define('ALL_TEXT',__('Todo','templatic'));


//success.php
define('POSTED_SUCCESS_TITLE',__('Your listing is submitted successfully!','templatic'));
define('RENEW_SUCCESS_TITLE',__('Your listing is renewed successfully!','templatic'));
define('POSTED_SUCCESS_MSG',__('<p>Thank you, your information has been received.</p><p><a href="[#submited_information_link#]" >View your listing &raquo;</a></p><p>Thank you for visiting us at [#site_name#].</p>','templatic'));
define('POSTED_SUCCESS_PREBANK_MSG',__('<p>Thank you, your request received successfully.</p><p>To publish the listing please transfer the amount of <u>[#order_amt#] </u> at our bank with the following information :</p>
<p>Bank Name : [#bank_name#]</p><p>Account Number : [#account_number#]</p><br><p>Please include the ID as reference : [#orderId#]</p><p><a href="[#submited_information_link#]" >View your submitted listing</a><br>
<p>Thank you for visit at [#site_name#].</p>','templatic'));
//cancel.php
define('PAY_CANCELATION_TITLE',__('The listing process was canceled','templatic'));
define('PAY_CANCEL_MSG',__('<p>We could not process your listing as expected. You can try adding a listing or an event listing again. If the cancellation happens repeatedly, contact the administrator. Thank you for visiting us.</p>','templatic'));

//return.php
define('PAYMENT_SUCCESS_TITLE',__('Payment successful!','templatic'));
define('PAYMENT_SUCCESS_MSG',__('<h4>Your payment has been received successfully and your listing is published.</h4>
<p><a href="[#submited_information_link#]" >View your listing &raquo;</a></p><h5>Thank you for becoming a member at [#site_name#].</h5>','templatic'));
define('INVALID_TRANSACTION_TITLE',__('<h4>Inavalid transation</h4>','templatic'));
define('INVALID_TRANSACTION_CONTENT',__('<p>There is some error occured while transaction is being process, please try again.</p>','templatic'));
define('AUTHENTICATION_CONTENT',__('<p>Authentication fail, invalid transaction id generated.</p>','templatic'));

define('FORGOT_PW_TEXT',__('Forgot Password?','templatic'));
define('SIGN_IN_BUTTON',__('Sign In','templatic'));

//dashboard.php
define('DASHBOARD_TEXT',__('Panel','templatic'));
define('EDIT_PROFILE_PAGE_TITLE',__('Información de Cuenta','templatic'));
define('CHANGE_PW_TEXT',__('Cambiar Contraseña','templatic'));

//sidebar.php
define('MY_ACCOUNT_TEXT',__('Mi Cuenta','templatic'));

//header.php
define('WELCOME_TEXT',__('Bienvenido ','templatic'));
define('GUEST_TEXT',__('Invitado','templatic'));
define('LOGOUT_TEXT',__('Cerrar Sesión','templatic'));
define('SIGN_IN_TEXT',__('Iniciar Sesión','templatic'));
define('SIGN_UP_TEXT',__('Registrarse','templatic'));



//library/functions/meta_boxes.php
define('YES_VERIFIED',__('Verified','templatic'));
define('IP_IS',__('IP Detected for this post is :','templatic'));
define('POST_VERIFIED_TEXT',__('This post is verified.','templatic'));
define('NO_CLAIM',__('This entry has not yet been claimed by anyone.','templatic'));
define('CLAIM_THIS',__('Claim this post','templatic'));
define('REMOVE_CLAIM_REQUEST',__('Remove Claim Request','templatic'));
define('VERIFY_THIS',__('Verify this post','templatic'));
define('UNBLOCK_IP',__('Unblock IP.','templatic'));
define('BLOCK_IP',__('Block IP','templatic'));
define('IP_NOT_DETECTED',__('IP address not detected for this post.','templatic'));
define('SUBMITTED_IP',__('Submitted from IP','templatic'));
define('DELETE_CONFIRM_ALERT',__('Are you sure you want to delete?','templatic'));
define('ENTRY_DELETED',__('Entry deleted.','templatic'));
define('NO_ACTION',__('No action taken.','templatic'));
define('IP_BLOCKED',__('This IP is already blocked.','templatic'));

/*---monetize/manage_settings/admin_price_add.php--*/

define('PRICE_MOD_TITLE',__('Add a new price package','templatic')); 
define('PRICE_EDIT_MOD_TITLE',__('Here you can edit price detail','templatic')); 
define('PRICE_BACK_LABLE',__('&laquo; Back to manage price package list','templatic')); 
define('PRICE_MOD_MSG',__('<p class="notes_spec">Make a new price package here. You can set the validity period, status and customize pricing for featured listings within this package.</p>','templatic')); 
define('PRICE_POST_TYPE_LABLE',__('Post type','templatic')); 
define('PRICE_SETTING_TITLE',__('Price settings','templatic')); 
define('PRICE_POST_TYPE_NOTE',__('Select post type','templatic')); 
define('PRICE_CAT_TITLE',__('Apply this package to these categories','templatic')); 
define('PRICE_POST_CAT_NOTE',__('<br/><p>This pricing package will be displayed on "Add a place" and "Add an event" page only when the above check-marked categories are opted by the user.<br><br><b>Note:</b> Category-specific price amount will be added / deducted from the final billing(<a href="'.site_url().'/wp-admin/admin.php?page=manage_settings#option_display_icons">Set category-specific charges here</a>)</p>','templatic')); 
define('CAT_SECTION_TITLE',__('You can specify individual price-weightage for each category and their custom icons. <br /><br /><strong>Note:</strong> Use + and - signs to add and deduct the specified amount (from the price package amount) when that particular category is chosen by the user. (Check <a href="'.site_url().'/wp-admin/admin.php?page=manage_settings#option_display_price" target="blank">Price package settings</a>, too)','templatic')); 
define('PRICE_PACK_TITLE',__('Price package title','templatic')); 
define('PRICE_TITLE_NOTE',__('The name of the package that will appear in the front/back-end.','templatic')); 
define('PRICE_AMOUNT_TITLE',__('Package cost','templatic')); 
define('PRICE_AMOUNT_NOTE',__('Set the package cost here. eg. 50','templatic')); 
define('PRICE_DESC_TITLE',__('Description of the price package','templatic')); 
define('PRICE_DESC_NOTE',__('Describe this price package in a few words. This field is optional.','templatic')); 
define('VALIDITY_TITLE',__('Billing period','templatic')); 
define('VALIDITY_NOTE',__('Set the time period of this package.','templatic')); 
define('REC_TITLE',__('Recurring?','templatic')); 
define('REC_NOTE',__('Specify whether users will be automatically charged after the billing period gets over.(<strong>NOTE: Only work for paypal</strong>)','templatic')); 
define('CHARGES_USER',__('Charge users every','templatic')); 
define('BILLING_PERIOD_TITLE',__('Billing period','templatic')); 
define('BILLING_PERIOD_NOTE',__('Set the billing period of this package.','templatic')); 
define('BILLING_CYCLE_TITLE',__('Maximum times recurring billing occurs','templatic')); 
define('BILLING_CYCLE_NOTE',__('Specify how many times the recurring billing process should occur. eg. 5','templatic')); 
define('FEATURE_HEAD_TITLE',__('Price settings for Featured listings','templatic')); 
define('FEATURE_HEAD_NOTE',__('<p class="notes_spec">You might want to charge users extra amount for featured listings within this price package.</p> ','templatic')); 
define('FEATURE_STATUS_NOTE',__('Activate/deactivate additional price settings for &raquofeatured&raquo listings.','templatic')); 
define('FEATURE_AMOUNT_TITLE',__('Amount to be charged<br><small>(for featuring on homepage)</small>','templatic')); 
define('FEATURE_AMOUNT_NOTE',__('This is the price you wish to charge extra for featuring a place or an event listing on the homepage. eg. 20','templatic')); 
define('FEATURE_CAT_TITLE',__('Amount to be charged<br><small>(for featuring on categories page)</small>','templatic')); 
define('FEATURE_CAT_NOTE',__('This is the price you wish to charge extra for a place or an event listing on category pages. eg. 12','templatic')); 
define('STATUS_NOTE',__('This setting will activate / deactivate this price package.','templatic'));
define('MY_FAVOURITE_TEXT',__('My Favorites','templatic'));
define('ADD_FAVOURITE_TEXT',__('Add to Favorites','templatic'));
define('REMOVE_FAVOURITE_TEXT',__('Remove from Favorites','templatic'));
define('IMAGE_NOT_AVAILABLE_TEXT',__('Image Not Available','templatic'));
define('NOLISTING_TEXT',__('<strong>No place available under this category.</strong>','templatic'));
define('NO_EVENT_TEXT',__('<strong>No event available right now</strong>','templatic'));
define('LOCATION',__('Location : ','templatic'));
define('READ_MORE',__('Location : ','templatic'));
define('NO_RECORD',__('No record inserted ','templatic'));

/*---monetize/manage_settings/admin_price_package.php--*/

define('PRICE_TITLE',__('Título','templatic'));
define('PRICE_PKG',__('Costo del Paquete ','templatic'));
define('PRICE_STATUS',__('Estado ','templatic'));
define('PRICE_DESC',__('Descripción ','templatic'));
define('PRICE_VAL',__('Billing period ','templatic'));
define('PRICE_TYPE',__('Post type ','templatic'));
define('PRICE_CAT',__('Categoria de Post','templatic'));
define('REC_PAY',__('Pago recurrente','templatic'));
define('NO_CTA_PKG',__('Show this price package even when no category selected.','templatic'));
define('PAYPAL_MSG',__('Procesando para PayPal, espere por favor...','templatic','templatic'));

/*Submit_place.php*/
define('IP_BLOCK',__('Tu IP está bloqueada. No puedes agregar un lugar.','templatic'));
define('FEATURED_H',__('Yes &sbquo; feature this listing on homepage.','templatic'));
define('FEATURED_C',__('Yes &sbquo; feature this listing on category pages.','templatic'));
define('ENTER_PLACE',__('1. Enter Place Listing Details','templatic'));
define('PREVIEW_PLACE',__('2. Preview listing &amp; Payment','templatic'));
define('SUCCESS_PLACE',__('3. Place Successful','templatic'));
define('AUTHORISE_NET_MSG',__('Authorised.net payment processing....','templatic'));

/*submit_event.php */
define('ENTER_EVENT',__('1. Enter Event Listing Details','templatic'));
define('FEATURED_H_EVENT',__('Yes &sbquo; feature this event on homepage.','templatic'));
define('FEATURED_C_EVENT',__('Yes &sbquo; feature this event on category pages.','templatic'));
define('PREVIEW_EVENT',__('2. Preview Event &amp; Payment','templatic'));
define('SUCCESS_EVENT',__('3. Event Successful','templatic'));

/*tpl_splash.php*/
define('SELECT_CITY_TPL',__('Seleccionar Ciudad','templatic'));
define('SELECT_CITY_DESC_TPL',__('Seleccione una ciudad para mostrar lugares &amp; eventos relacionados con la ciudad.','templatic'));

/***Backend monetize/manage_settings/admin_manage_permission.php***/
define('DISABLE_ACCESS_TEXT',__('Deshabilitar el acceso de back-end para','templatic'));
define('SAVE_ALL_CHANGES_TEXT',__('Guardar Todos los Cambios','templatic'));
define('RESET_OPTIONS_TEXT',__('Resetear Opciones','templatic'));
define('MANAGE_PERMISSION_TEXT',__('Administrar permisos','templatic'));
define('MANAGE_CAT_SET_TEXT',__('Administrar la configuración de categoría','templatic'));

/**Payment options **/
define('MERCHANT_ID_TEXT',__('Merchant Id','templatic'));
define('CANCEL_URL_TEXT',__('Cancel Url','templatic'));
define('RETURN_URL_TEXT',__('Return Url','templatic'));
define('GCHECKOUT_TEXT',__('Google Checkout','templatic'));
define('NOTIFY_URL_TEXT',__('Notify Url','templatic'));
define('LOGIN_ID_TEXT',__('Login ID','templatic'));
define('LOGIN_ID_NOTE',__('Example : yourname@domain.com','templatic'));
define('TRANS_KEY_TEXT',__('Transaction Key','templatic'));
define('TRANS_KEY_NOTE',__('Ejemplo : 1234567890','templatic'));
define('INSTANT_ID_TEXT',__('Instant Id','templatic'));
define('ACCOUNT_ID_TEXT',__('Account Id','templatic'));
define('ACCOUNT_ID_NOTE2',__('"Enter your bank Account ID','templatic'));
define('VENDOR_ID_TEXT',__('Vendor Id','templatic'));
define('VENDOR_ID_NOTE',__('Enter Vendor ID Example : 1303908','templatic'));
define('ACCOUNT_ID_NOTE',__('Example: 12345','templatic'));
define('INSTANT_ID_NOTE',__('Example: 123456','templatic'));
define('WORLD_PAY_TEXT',__('Worldpay','templatic'));
define('BANK_INFO_TEXT',__('Bank Information','templatic'));
define('PRE_BANK_TRANSFER_TEXT',__('Pre Bank Transfer','templatic'));
define('BANK_INFO_NOTE',__('Enter the bank name to which you want to transfer payment','templatic'));
define('PAY_CASH_TEXT',__('Pay Cash On Arrival','templatic'));
?>