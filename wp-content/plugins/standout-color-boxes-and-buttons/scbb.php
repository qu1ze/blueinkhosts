<?php
/*
Plugin Name: Standout Color Boxes and Buttons
Plugin URI: http://www.jimmyscode.com/wordpress/standout-color-boxes-buttons/
Description: This plugin implements colored content boxes and buttons as described in a Studiopress blog post as shortcodes.
Version: 0.5.0
Author: Jimmy Pe&ntilde;a
Author URI: http://www.jimmyscode.com/
License: GPLv2 or later
*/
// plugin constants
define('SCBB_VERSION', '0.5.0');
define('SCBB_PLUGIN_NAME', 'Standout Color Boxes and Buttons');
define('SCBB_SLUG', 'standout-color-boxes-and-buttons');
define('SCBB_LOCAL', 'scbb');
define('SCBB_OPTION', 'scbb');
/* default values */
define('SCBB_DEFAULT_ENABLED', true);
define('SCBB_DEFAULT_COLOR', '');
define('SCBB_DEFAULT_ROUNDED', true);
define('SCBB_DEFAULT_SHADOW', false);
define('SCBB_DEFAULT_URL', '');
define('SCBB_DEFAULT_CUSTOM_CSS', '');
define('SCBB_DEFAULT_NOFOLLOW', true);
define('SCBB_DEFAULT_SHOW', false);
define('SCBB_DEFAULT_NEWWINDOW', false);
define('SCBB_AVAILABLE_COLORS', 'blue,gray,green,purple,red,yellow,black,white,orange,pink,bluebell');
/* option array member names */
define('SCBB_DEFAULT_ENABLED_NAME', 'enabled');
define('SCBB_DEFAULT_COLOR_NAME', 'color');
define('SCBB_DEFAULT_ROUNDED_NAME', 'rounded');
define('SCBB_DEFAULT_SHADOW_NAME', 'dropshadow');
define('SCBB_DEFAULT_URL_NAME', 'href');
define('SCBB_DEFAULT_CUSTOM_CSS_NAME', 'customcss');
define('SCBB_DEFAULT_NOFOLLOW_NAME', 'nofollow');
define('SCBB_DEFAULT_SHOW_NAME', 'show');
define('SCBB_DEFAULT_NEWWINDOW_NAME', 'opennewwindow');

// oh no you don't
if (!defined('ABSPATH')) {
  wp_die(__('Do not access this file directly.', SCBB_LOCAL));
}

// delete option when plugin is uninstalled
register_uninstall_hook(__FILE__, 'uninstall_scbb_plugin');
function uninstall_scbb_plugin() {
  delete_option(SCBB_OPTION);
}

// localization to allow for translations
add_action('init', 'scbb_translation_file');
function scbb_translation_file() {
  $plugin_path = plugin_basename(dirname(__FILE__)) . '/translations';
  load_plugin_textdomain(SCBB_LOCAL, '', $plugin_path);
  register_scbb_style();
}
// tell WP that we are going to use new options
add_action('admin_init', 'scbb_options_init');
function scbb_options_init() {
  register_setting('scbb_options', SCBB_OPTION, 'scbb_validation');
  register_scbb_admin_style();
  register_scbb_admin_script();
}
// validation function
function scbb_validation($input) {
  // sanitize color
  $input[SCBB_DEFAULT_COLOR_NAME] = sanitize_text_field($input[SCBB_DEFAULT_COLOR_NAME]);
  // sanitize url
  $input[SCBB_DEFAULT_URL_NAME] = esc_url($input[SCBB_DEFAULT_URL_NAME]);
  // sanitize custom css
  $input[SCBB_DEFAULT_CUSTOM_CSS_NAME] = sanitize_text_field($input[SCBB_DEFAULT_CUSTOM_CSS_NAME]);
  return $input;

}
// add Settings sub-menu
add_action('admin_menu', 'scbb_plugin_menu');
function scbb_plugin_menu() {
  add_options_page(SCBB_PLUGIN_NAME, SCBB_PLUGIN_NAME, 'manage_options', SCBB_SLUG, 'scbb_page');
}
// plugin settings page
// http://planetozh.com/blog/2009/05/handling-plugins-options-in-wordpress-28-with-register_setting/
// http://www.onedesigns.com/tutorials/how-to-create-a-wordpress-theme-options-page
function scbb_page() {
  // check perms
  if (!current_user_can('manage_options')) {
    wp_die(__('You do not have sufficient permission to access this page', SCBB_LOCAL));
  }
?>
  <div class="wrap">
    <?php screen_icon(); ?>
    <h2><?php echo SCBB_PLUGIN_NAME; ?></h2>
    <form method="post" action="options.php">
      <div>You are running plugin version <strong><?php echo SCBB_VERSION; ?></strong>.</div>
      <?php settings_fields('scbb_options'); ?>
      <?php $options = scbb_getpluginoptions(); ?>
      <?php update_option(SCBB_OPTION, $options); ?>
      <table class="form-table" id="theme-options-wrap">
        <tr valign="top"><th scope="row"><strong><label title="<?php _e('Is plugin enabled? Uncheck this to turn it off temporarily.', SCBB_LOCAL); ?>" for="scbb[<?php echo SCBB_DEFAULT_ENABLED_NAME; ?>]"><?php _e('Plugin enabled?', SCBB_LOCAL); ?></label></strong></th>
		<td><input type="checkbox" id="scbb[<?php echo SCBB_DEFAULT_ENABLED_NAME; ?>]" name="scbb[<?php echo SCBB_DEFAULT_ENABLED_NAME; ?>]" value="1" <?php checked('1', $options[SCBB_DEFAULT_ENABLED_NAME]); ?> /></td>
        </tr>
	  <tr><td colspan="2"><?php _e('Is plugin enabled? Uncheck this to turn it off temporarily.', SCBB_LOCAL); ?></td></tr>
        <tr valign="top"><th scope="row"><strong><label title="<?php _e('Select the color you would like to use as the default.', SCBB_LOCAL); ?>" for="scbb[<?php echo SCBB_DEFAULT_COLOR_NAME; ?>]"><?php _e('Default color', SCBB_LOCAL); ?></label></strong></th>
		<td><select id="scbb[<?php echo SCBB_DEFAULT_COLOR_NAME; ?>]" name="scbb[<?php echo SCBB_DEFAULT_COLOR_NAME; ?>]">
                <?php $colors = explode(",", SCBB_AVAILABLE_COLORS);
                      foreach($colors as $color) {
				echo '<option value="' . $color . '" ' . selected($color, $options[SCBB_DEFAULT_COLOR_NAME])  . '>' . $color . '</option>';
                      } ?>
            </select></td>
        </tr>
        <tr><td colspan="2"><?php _e('Select the color you would like to use as the default if no color is otherwise specified.', SCBB_LOCAL); ?></td></tr>
        <tr valign="top"><th scope="row"><strong><label title="<?php _e('Check this box to use rounded corner CSS.', SCBB_LOCAL); ?>" for="scbb[<?php echo SCBB_DEFAULT_ROUNDED_NAME; ?>]"><?php _e('Enabled CSS rounded corners?', SCBB_LOCAL); ?></label></strong></th>
		<td><input type="checkbox" id="scbb[<?php echo SCBB_DEFAULT_ROUNDED_NAME; ?>]" name="scbb[<?php echo SCBB_DEFAULT_ROUNDED_NAME; ?>]" value="1" <?php checked('1', $options[SCBB_DEFAULT_ROUNDED_NAME]); ?> /></td>
        </tr>
	  <tr valign="top"><td colspan="2"><?php _e('Check this box to use rounded corner CSS. You can override this at the shortcode level.', SCBB_LOCAL); ?></td></tr>
        <tr valign="top"><th scope="row"><strong><label title="<?php _e('Check this box to use drop shadow CSS.', SCBB_LOCAL); ?>" for="scbb[<?php echo SCBB_DEFAULT_SHADOW_NAME; ?>]"><?php _e('Enabled CSS drop shadow?', SCBB_LOCAL); ?></label></strong></th>
		<td><input type="checkbox" id="scbb[<?php echo SCBB_DEFAULT_SHADOW_NAME; ?>]" name="scbb[<?php echo SCBB_DEFAULT_SHADOW_NAME; ?>]" value="1" <?php checked('1', $options[SCBB_DEFAULT_SHADOW_NAME]); ?> /></td>
        </tr>
	  <tr valign="top"><td colspan="2"><?php _e('Check this box to use drop shadow CSS. You can override this at the shortcode level.', SCBB_LOCAL); ?></td></tr>
        <tr valign="top"><th scope="row"><strong><label title="<?php _e('Enter default URL to use for color buttons.', SCBB_LOCAL); ?>" for="scbb[<?php echo SCBB_DEFAULT_URL_NAME; ?>]"><?php _e('Default button URL', SCBB_LOCAL); ?></label></strong></th>
		<td><input type="text" id="scbb[<?php echo SCBB_DEFAULT_URL_NAME; ?>]" name="scbb[<?php echo SCBB_DEFAULT_URL_NAME; ?>]" value="<?php echo $options[SCBB_DEFAULT_URL_NAME]; ?>" /></td>
        </tr>
	  <tr valign="top"><td colspan="2"><?php _e('Enter default URL to use for color buttons. This URL will be used if you do not override it at the shortcode level.', SCBB_LOCAL); ?></td></tr>
        <tr valign="top"><th scope="row"><strong><label title="<?php _e('Check this box to add rel=nofollow to button links.', SCBB_LOCAL); ?>" for="scbb[<?php echo SCBB_DEFAULT_NOFOLLOW_NAME; ?>]"><?php _e('Nofollow button link?', SCBB_LOCAL); ?></label></strong></th>
		<td><input type="checkbox" id="scbb[<?php echo SCBB_DEFAULT_NOFOLLOW_NAME; ?>]" name="scbb[<?php echo SCBB_DEFAULT_NOFOLLOW_NAME; ?>]" value="1" <?php checked('1', $options[SCBB_DEFAULT_NOFOLLOW_NAME]); ?> /></td>
        </tr>
	  <tr valign="top"><td colspan="2"><?php _e('Check this box to add rel="nofollow" to button links only. You can override this at the shortcode level.', SCBB_LOCAL); ?></td></tr>
        <tr valign="top"><th scope="row"><strong><label title="<?php _e('Check this box to open links in a new window (color buttons only).', SCBB_LOCAL); ?>" for="scbb[<?php echo SCBB_DEFAULT_NEWWINDOW_NAME; ?>]"><?php _e('Open links in new window?', SCBB_LOCAL); ?></label></strong></th>
		<td><input type="checkbox" id="scbb[<?php echo SCBB_DEFAULT_NEWWINDOW_NAME; ?>]" name="scbb[<?php echo SCBB_DEFAULT_NEWWINDOW_NAME; ?>]" value="1" <?php checked('1', $options[SCBB_DEFAULT_NEWWINDOW_NAME]); ?> /></td>
        </tr>
	  <tr valign="top"><td colspan="2"><?php _e('Check this box to open links in a new window (color buttons only). You can override this at the shortcode level.', SCBB_LOCAL); ?></td></tr>
      <tr valign="top"><th scope="row"><strong><label title="<?php _e('Enter custom CSS', SCBB_LOCAL); ?>" for="scbb[<?php echo SCBB_DEFAULT_CUSTOM_CSS_NAME; ?>]"><?php _e('Enter custom CSS', SCBB_LOCAL); ?></label></strong></th>
		<td><textarea rows="12" cols="75" id="scbb[<?php echo SCBB_DEFAULT_CUSTOM_CSS_NAME; ?>]" name="scbb[<?php echo SCBB_DEFAULT_CUSTOM_CSS_NAME; ?>]"><?php echo $options[SCBB_DEFAULT_CUSTOM_CSS_NAME]; ?></textarea></td>
		</tr>
	  <tr valign="top"><td colspan="2"><?php _e('If you use your own custom class names, enter the CSS here. Use the custom class name (minus the "scbb-button" or "scbb-content-box" prefix) in the shortcode or when calling the function in PHP.', SCBB_LOCAL); ?></td></tr>
	  </table>
      <?php submit_button(); ?>
    </form>
    <h3>Plugin Arguments and Defaults</h3>
    <h4>Color Boxes</h4>
    <table class="widefat">
      <thead>
        <tr>
          <th title="<?php _e('The name of the parameter', SCBB_LOCAL); ?>"><?php _e('Argument', SCBB_LOCAL); ?></th>
		  <th title="<?php _e('Is this parameter required?', SCBB_LOCAL); ?>"><?php _e('Required?', SCBB_LOCAL); ?></th>
          <th title="<?php _e('What data type this parameter accepts', SCBB_LOCAL); ?>"><?php _e('Type', SCBB_LOCAL); ?></th>
          <th title="<?php _e('What, if any, is the default if no value is specified', SCBB_LOCAL); ?>"><?php _e('Default Value', SCBB_LOCAL); ?></th>
        </tr>
      </thead>
      <tbody>
    <?php $plugin_defaults_keys = array_keys(scbb_colorbox_shortcode_defaults()); 
					$plugin_defaults_values = array_values(scbb_colorbox_shortcode_defaults()); 
					$colorboxrequired = scbb_box_required_parameters();
					for($i=0; $i<count($plugin_defaults_keys);$i++) { ?>
        <tr>
          <td><?php echo $plugin_defaults_keys[$i]; ?></td>
					<td><?php echo $colorboxrequired[$i]; ?></td>
          <td><?php echo gettype($plugin_defaults_values[$i]); ?></td>
          <td><?php 
						if ($plugin_defaults_values[$i] === true) {
							echo 'true';
						} elseif ($plugin_defaults_values[$i] === false) {
							echo 'false';
						} elseif ($plugin_defaults_values[$i] === '') {
							echo '<em>(this value is blank by default)</em>';
						} else {
							echo $plugin_defaults_values[$i];
						} ?></td>
        </tr>
    <?php } ?>
    </tbody>
    </table>
    <h4>Color Buttons</h4>
    <table class="widefat">
      <thead>
        <tr>
          <th title="<?php _e('The name of the parameter', SCBB_LOCAL); ?>"><?php _e('Argument', SCBB_LOCAL); ?></th>
		  <th title="<?php _e('Is this parameter required?', SCBB_LOCAL); ?>"><?php _e('Required?', SCBB_LOCAL); ?></th>
          <th title="<?php _e('What data type this parameter accepts', SCBB_LOCAL); ?>"><?php _e('Type', SCBB_LOCAL); ?></th>
          <th title="<?php _e('What, if any, is the default if no value is specified', SCBB_LOCAL); ?>"><?php _e('Default Value', SCBB_LOCAL); ?></th>
        </tr>
      </thead>
      <tbody>
    <?php $plugin_defaults_keys = array_keys(scbb_colorbutton_shortcode_defaults());
					$plugin_defaults_values = array_values(scbb_colorbutton_shortcode_defaults());
					$colorbuttonrequired = scbb_button_required_parameters();
					for($i=0; $i<count($plugin_defaults_keys);$i++) { ?>
        <tr>
          <td><?php echo $plugin_defaults_keys[$i]; ?></td>
					<td><?php echo $colorbuttonrequired[$i]; ?></td>
          <td><?php echo gettype($plugin_defaults_values[$i]); ?></td>
          <td><?php 
						if ($plugin_defaults_values[$i] === true) {
							echo 'true';
						} elseif ($plugin_defaults_values[$i] === false) {
							echo 'false';
						} elseif ($plugin_defaults_values[$i] === '') {
							echo '<em>(this value is blank by default)</em>';
						} else {
							echo $plugin_defaults_values[$i];
						} ?></td>
        </tr>
    <?php } ?>
    </tbody>
    </table>
    <?php screen_icon('edit-comments'); ?>
    <h3>Support</h3>
    <div class="support">
    If you like this plugin, please <a href="http://wordpress.org/support/view/plugin-reviews/<?php echo SCBB_SLUG; ?>/">rate it on WordPress.org</a> and click the "Works" button so others know it will work for your WordPress version. For support please visit the <a href="http://wordpress.org/support/plugin/<?php echo SCBB_SLUG; ?>">forums</a>. <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=7EX9NB9TLFHVW"><img src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif" alt="Donate with PayPal" title="Donate with PayPal" width="92" height="26" /></a>
    </div>
  </div>
  <?php }
// shortcodes for boxes and buttons
add_shortcode('color-box', 'color_box_shortcode');
add_shortcode('color-button', 'color_button_shortcode');
// -------------------------------------------------------------------
// shortcode output
// -------------------------------------------------------------------
function color_box_shortcode($atts, $content = null) {
  // get parameters
  extract(shortcode_atts(scbb_colorbox_shortcode_defaults(), $atts));
  // plugin is enabled/disabled from settings page only
  $options = scbb_getpluginoptions();
  $enabled = $options[SCBB_DEFAULT_ENABLED_NAME];

  // ******************************
  // derive shortcode values from constants
  // ******************************
  $temp_color = constant('SCBB_DEFAULT_COLOR_NAME');
  $boxcolor = $$temp_color;
  $temp_rounded = constant('SCBB_DEFAULT_ROUNDED_NAME');
  $rounded = $$temp_rounded;
  $temp_shadow = constant('SCBB_DEFAULT_SHADOW_NAME');
  $dropshadow = $$temp_shadow;
  $temp_show = constant('SCBB_DEFAULT_SHOW_NAME');
  $show = $$temp_show;

  // ******************************
  // sanitize user input
  // ******************************
  $boxcolor = sanitize_html_class($boxcolor);
  $rounded = (bool)$rounded;
  $dropshadow = (bool)$dropshadow;
  $show = (bool)$show;

  // ******************************
  // check for parameters, then settings, then defaults
  // ******************************
  if ($enabled) {
    if ($content === null) {
      // what is the point of a color box w/ no content?
      $enabled = false;
      $output = '<!-- ' . SCBB_PLUGIN_NAME . ': plugin is disabled. Either you did not pass a necessary setting to the plugin, or did not configure a default. Check Settings page. -->';
    } else {
      // plugin enabled and there is content
      // check for overriden parameters, if nonexistent then get from DB
      if ($rounded == SCBB_DEFAULT_ROUNDED) {
        $rounded = $options[SCBB_DEFAULT_ROUNDED_NAME];
        if ($rounded == false) {
          $rounded = SCBB_DEFAULT_ROUNDED;
        }
      } // end rounded
      if ($dropshadow == SCBB_DEFAULT_SHADOW) {
        $dropshadow = $options[SCBB_DEFAULT_SHADOW_NAME];
        if ($dropshadow == false) {
          $dropshadow = SCBB_DEFAULT_SHADOW;
        }
      } // end dropshadow
      // check if color value was passed
      if (($boxcolor == false) || ($boxcolor == SCBB_DEFAULT_COLOR)) {
        // not passed by shortcode, use default
        $boxcolor = $options[SCBB_DEFAULT_COLOR_NAME];
      } else { // a value was passed, check if it is one of the available colors. if not, assume a custom css name and load custom.css
        $colors = explode(",", SCBB_AVAILABLE_COLORS);
        if (!in_array($boxcolor, $colors)) {
          // write CSS to custom.css file
          $myFile = dirname(__FILE__) . '/css/custom.css';
	  $fh = @fopen($myFile, 'w+');
	  @fwrite($fh, $options[SCBB_DEFAULT_CUSTOM_CSS_NAME]);
	  @fclose($fh);
	  // enqueue custom css file
          scbb_custom_styles();
        }
      } // end color

      // enqueue CSS on pages w/ shortcode/function
      color_box_styles();

      $output = '<div class="scbb-content-box-' . $boxcolor . ($rounded ? ' scbb-rounded-corners' : '') . ($dropshadow ? ' scbb-dropshadow' : '') . '">' . do_shortcode(wp_kses_post($content)) . '</div>';
    } // end content check
  } else { // not enabled
    $output = '<!-- ' . SCBB_PLUGIN_NAME . ': plugin is disabled. Either you did not pass a necessary setting to the plugin, or did not configure a default. Check Settings page. -->';
  } // end enabled check

  if ($show) {
    echo $output;
  } else {
    return $output;
  }
} // end color box shortcode function

function color_button_shortcode($atts, $content = null) {
  // get parameters
  extract(shortcode_atts(scbb_colorbutton_shortcode_defaults(), $atts));
  // plugin is enabled/disabled from settings page only
  $options = scbb_getpluginoptions();
  $enabled = $options[SCBB_DEFAULT_ENABLED_NAME];

  // ******************************
  // derive shortcode values from constants
  // ******************************
  $temp_color = constant('SCBB_DEFAULT_COLOR_NAME');
  $buttoncolor = $$temp_color;
  $temp_rounded = constant('SCBB_DEFAULT_ROUNDED_NAME');
  $rounded = $$temp_rounded;
  $temp_shadow = constant('SCBB_DEFAULT_SHADOW_NAME');
  $dropshadow = $$temp_shadow;
  $temp_show = constant('SCBB_DEFAULT_SHOW_NAME');
  $show = $$temp_show;
  $temp_url = constant('SCBB_DEFAULT_URL_NAME');
  $url = $$temp_url;
  $temp_window = constant('SCBB_DEFAULT_NEWWINDOW_NAME');
  $opennewwindow = $$temp_window;
  $temp_nofollow = constant('SCBB_DEFAULT_NOFOLLOW_NAME');
  $nofollow = $$temp_nofollow;

  // ******************************
  // sanitize user input
  // ******************************
  $url = esc_url($url);
  $buttoncolor = sanitize_text_field($buttoncolor);
  $rounded = (bool)$rounded;
  $dropshadow = (bool)$dropshadow;
  $nofollow = (bool)$nofollow;
  $opennewwindow = (bool)$opennewwindow;
  $show = (bool)$show;

  // ******************************
  // check for parameters, then settings, then defaults
  // ******************************
  if ($enabled) {
    if ($content === null) {
      // what is the point of a color button w/ no content?
      $enabled = false;
      $output = '<!-- ' . SCBB_PLUGIN_NAME . ': plugin is disabled. Either you did not pass a necessary setting to the plugin, or did not configure a default. Check Settings page. -->';
    } elseif ($url === SCBB_DEFAULT_URL) { // no url passed to function, try settings page
        $url = $options[SCBB_DEFAULT_URL_NAME];
        if (($url === SCBB_DEFAULT_URL) || ($url === false)) { // no url on settings page either
          $enabled = false;
          $output = '<!-- ' . SCSS3B_PLUGIN_NAME . ': plugin is disabled. Either you did not pass a necessary setting to the plugin, or did not configure a default. Check Settings page. -->';
        }
    } else {
      // plugin is enabled and there is content
      // check for overriden parameters, if nonexistent then get from DB
      if ($rounded === SCBB_DEFAULT_ROUNDED) {
        $rounded = $options[SCBB_DEFAULT_ROUNDED_NAME];
        if ($rounded === false) {
          $rounded = SCBB_DEFAULT_ROUNDED;
        }
      } // end rounded
      if ($dropshadow === SCBB_DEFAULT_SHADOW) {
        $dropshadow = $options[SCBB_DEFAULT_SHADOW_NAME];
        if ($dropshadow === false) {
          $dropshadow = SCBB_DEFAULT_SHADOW;
        }
      } // end dropshadow
      if ($nofollow === SCBB_DEFAULT_NOFOLLOW) {
        $nofollow = $options[SCBB_DEFAULT_NOFOLLOW_NAME];
        if ($nofollow === false) {
          $nofollow = SCBB_DEFAULT_NOFOLLOW;
        }
      } // end nofollow
      if ($opennewwindow === SCBB_DEFAULT_NEWWINDOW) {
        $opennewwindow = $options[SCBB_DEFAULT_NEWWINDOW_NAME];
        if ($opennewwindow === false) {
          $opennewwindow = SCBB_DEFAULT_NEWWINDOW;
        }
      } // end new window
      // check if color value was passed
      if (($buttoncolor == false) || ($buttoncolor == SCBB_DEFAULT_COLOR)) {
        // not passed by shortcode, use default
        $buttoncolor = $options[SCBB_DEFAULT_COLOR_NAME];
      } else { // a value was passed, check if it is one of the available colors. if not, assume a custom css name and load custom.css
        $colors = explode(",", SCBB_AVAILABLE_COLORS);
        if (!in_array($buttoncolor, $colors)) {
          // write CSS to custom.css file
          $myFile = dirname(__FILE__) . '/css/custom.css';
	  $fh = @fopen($myFile, 'w+');
	  @fwrite($fh, $options[SCBB_DEFAULT_CUSTOM_CSS_NAME]);
	  @fclose($fh);
	  // enqueue custom css file
	  scbb_custom_styles();
        }
      } // end color

      // enqueue CSS on pages w/ shortcode/function
      color_box_styles();

      $output = '<a' . ($opennewwindow ? ' onclick="window.open(this.href); return false;" onkeypress="window.open(this.href); return false;" ' : ' ') . 'class="scbb-button-' . $buttoncolor . ($rounded ? ' scbb-rounded-corners' : '') . ($dropshadow ? ' scbb-dropshadow' : '') . '" href="' . $url . '"' . ($nofollow ? ' rel="nofollow"' : '') . '>' . do_shortcode(wp_kses_post(force_balance_tags($content))) . '</a>';
    } // end content check
  } else { // not enabled
    $output = '<!-- ' . SCBB_PLUGIN_NAME . ': plugin is disabled. Either you did not pass a necessary setting to the plugin, or did not configure a default. Check Settings page. -->';
  } // end enabled check
  if ($show) {
    echo $output;
  } else {
    return $output;
  }
} // end color button shortcode function
// show admin messages to plugin user
add_action('admin_notices', 'scbb_showAdminMessages');
function scbb_showAdminMessages() {
  // http://wptheming.com/2011/08/admin-notices-in-wordpress/
  global $pagenow;
  if (current_user_can('manage_options')) { // user has privilege
    if ($pagenow == 'options-general.php') {
      if ($_GET['page'] == SCBB_SLUG) { // we are on settings page
        $options = scbb_getpluginoptions();
	  if ($options != false) {
	    $enabled = $options[SCBB_DEFAULT_ENABLED_NAME];
	    if (!$enabled) {
	      echo '<div id="message" class="error">' . SCBB_PLUGIN_NAME . ' ' . __('is currently disabled.', SCBB_LOCAL) . '</div>';
	    }
	  } 
	}
    } // end page check
  } // end privilege check
} // end admin msgs function
// enqueue admin CSS if we are on the plugin options page
add_action('admin_head', 'insert_scbb_admin_css');
function insert_scbb_admin_css() {
  global $pagenow;
  if (current_user_can('manage_options')) { // user has privilege
    if ($pagenow == 'options-general.php') {
      if ($_GET['page'] == SCBB_SLUG) { // we are on settings page
        scbb_admin_styles();
      }
    }
  }
}
// add settings link on plugin page
// http://bavotasan.com/2009/a-settings-link-for-your-wordpress-plugins/
add_filter('plugin_action_links_' . plugin_basename(__FILE__), 'scbb_plugin_settings_link' );
function scbb_plugin_settings_link($links) { 
  $settings_link = '<a href="options-general.php?page=' . SCBB_SLUG . '">' . __('Settings', SCBB_LOCAL) . '</a>'; 
  array_unshift($links, $settings_link); 
  return $links; 
}
// enqueue/register the plugin CSS file
function color_box_styles() {
  wp_enqueue_style('scbb_style');
}
function register_scbb_style() {
  wp_register_style('scbb_style', 
    plugins_url(plugin_basename(dirname(__FILE__)) . '/css/scbb.css'), 
    array(), 
    SCBB_VERSION, 
    'all' );
}
// enqueue/register the admin CSS file
function scbb_admin_styles() {
  wp_enqueue_style('scbb_admin_style');
}
function register_scbb_admin_style() {
  wp_register_style('scbb_admin_style',
    plugins_url(plugin_basename(dirname(__FILE__)) . '/css/admin.css'),
    array(),
    SCBB_VERSION,
    'all');
}
// enqueue/register the custom CSS file
function scbb_custom_styles() {
  wp_register_style('scbb_custom_style',
    plugins_url(plugin_basename(dirname(__FILE__)) . '/css/custom.css'),
	array(),
	SCBB_VERSION . "_" . date('njYHis', filemtime(dirname(__FILE__) . '/css/custom.css')),
	'all');
  wp_enqueue_style('scbb_custom_style');
}
// enqueue/register the admin JS file
add_action('admin_enqueue_scripts', 'scbb_ed_buttons');
function scbb_ed_buttons($hook) {
  if (($hook == 'post-new.php') || ($hook == 'post.php')) {
    wp_enqueue_script('scbb_add_editor_button');
  }
}
function register_scbb_admin_script() {
  wp_register_script('scbb_add_editor_button',
    plugins_url(plugin_basename(dirname(__FILE__)) . '/js/editor_button.js'), 
    array('quicktags'), 
    SCBB_VERSION, 
    true);
}
// when plugin is activated, create options array and populate with defaults
register_activation_hook(__FILE__, 'scbb_activate');
function scbb_activate() {
  $options = scbb_getpluginoptions();
  update_option(SCBB_OPTION, $options);
}
// generic function that returns plugin options from DB
// if option does not exist, returns plugin defaults
function scbb_getpluginoptions() {
  return get_option(SCBB_OPTION, array(
  SCBB_DEFAULT_NOFOLLOW_NAME => SCBB_DEFAULT_NOFOLLOW, 
  SCBB_DEFAULT_URL_NAME => SCBB_DEFAULT_URL, 
  SCBB_DEFAULT_SHADOW_NAME => SCBB_DEFAULT_SHADOW, 
  SCBB_DEFAULT_ROUNDED_NAME => SCBB_DEFAULT_ROUNDED, 
  SCBB_DEFAULT_COLOR_NAME => SCBB_DEFAULT_COLOR, 
  SCBB_DEFAULT_ENABLED_NAME => SCBB_DEFAULT_ENABLED, 
  SCBB_DEFAULT_NEWWINDOW_NAME => SCBB_DEFAULT_NEWWINDOW, 
  SCBB_DEFAULT_CUSTOM_CSS_NAME => SCBB_DEFAULT_CUSTOM_CSS
  ));
}
// function to return shortcode defaults for color boxes
function scbb_colorbox_shortcode_defaults() {
  return array(
    SCBB_DEFAULT_COLOR_NAME => SCBB_DEFAULT_COLOR, 
    SCBB_DEFAULT_ROUNDED_NAME => SCBB_DEFAULT_ROUNDED, 
    SCBB_DEFAULT_SHADOW_NAME => SCBB_DEFAULT_SHADOW, 
    SCBB_DEFAULT_SHOW_NAME => SCBB_DEFAULT_SHOW
    );
}
// function to return parameter status for color boxes (required or not)
function scbb_box_required_parameters() {
	return array(
		'false', 
		'false', 
		'false', 
		'false'
	);
}
// function to return shortcode defaults for color buttons
function scbb_colorbutton_shortcode_defaults() {
  return array(
      SCBB_DEFAULT_URL_NAME => SCBB_DEFAULT_URL, 
      SCBB_DEFAULT_COLOR_NAME => SCBB_DEFAULT_COLOR, 
      SCBB_DEFAULT_ROUNDED_NAME => SCBB_DEFAULT_ROUNDED, 
      SCBB_DEFAULT_SHADOW_NAME => SCBB_DEFAULT_SHADOW, 
      SCBB_DEFAULT_NOFOLLOW_NAME => SCBB_DEFAULT_NOFOLLOW, 
      SCBB_DEFAULT_NEWWINDOW_NAME => SCBB_DEFAULT_NEWWINDOW, 
      SCBB_DEFAULT_SHOW_NAME => SCBB_DEFAULT_SHOW
      );
}
// function to return parameter status for color buttons (required or not)
function scbb_button_required_parameters() {
	return array(
		'true', 
		'false', 
		'false', 
		'false', 
		'false', 
		'false', 
		'false', 
	);
}
?>